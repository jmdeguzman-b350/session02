-- Connection to MySQL via Terminal: mysql -u root
	-- "-u" stands for username
	-- "root" is the default username for sql.
	-- "-p" stands for password
	-- An empty values is the default password for SQL.

-- List down the databases inside the DBMS.
SHOW DATABASES;

-- Note:
	-- SQL QUERIES is case insentive, but to easily identify the queries we usually use UPPERCASE.
	-- Make sure semi-colons (;) are added at the end of the sql syntax.

-- Create a database
CREATE DATABASE blog_db;

-- select the database.
USE blog_db;

-- Create user table
CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
    email VARCHAR(100) NOT NULL,
    password VARCHAR(300),
    datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id)
);

-- Create POSTS table
CREATE TABLE posts(
	id INT NOT NULL AUTO_INCREMENT,
    author_id INT NOT NULL,
    title VARCHAR(500) NOT NULL,
    content VARCHAR(5000),
    datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id), 
    CONSTRAINT fk_post_author_id
            FOREIGN KEY (author_id) REFERENCES users(id) 
                    ON UPDATE CASCADE
                    ON DELETE RESTRICT
);


-- Create POSTS_likes table
CREATE TABLE post_likes(
	id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
	PRIMARY KEY (id), 
    CONSTRAINT fk_post_likes_post_like_id
            FOREIGN KEY (post_likes_id) REFERENCES post_likes(id) 
                    ON UPDATE CASCADE
                    ON DELETE RESTRICT,
    CONSTRAINT fk_post_likes_users_user_id
            FOREIGN KEY (user_id) REFERENCES users(id) 
                    ON UPDATE CASCADE
                    ON DELETE RESTRICT,            

);